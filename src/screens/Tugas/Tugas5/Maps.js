import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken('pk.eyJ1IjoibGFuYTkiLCJhIjoiY2tjcmxkM3NrMWJlejJ6bWthYWp4OWlxbSJ9.QLUKH_bEtR27jl8AteG3eQ');

const coordinates = [
    [107.598827, -6.896191],
    [107.596198, -6.899688],
    [107.618767, -6.902226],
    [107.621095, -6.898690],
    [107.615698, -6.896741],
    [107.613544, -6.897713],
    [107.613697, -6.893795],
    [107.610714, -6.891356],
    [107.605468, -6.893124],
    [107.609180, -6.898013]
]

const Maps = () => {

    const [cord, setCord] = useState(coordinates);

    useEffect(() => {
        const getLocation = async() => {
            try{
                const permission = await MapboxGL.requestAndroidLocationPermissions();
            } catch (error) {
                console.log("getLocation err => "+error);
            }
        }
        getLocation();
    }, []);

    const renderAnnotation = (counter) => {
        const id = 'pointAnnotation'+counter;
        const coordinate = cord[counter];
        const title = 'Longitude: '+cord[counter][0]+' Latitude: '+cord[counter][1];

        return(
            <MapboxGL.PointAnnotation
                key={id}
                id={id}
                coordinate={coordinate}
            >
                <MapboxGL.Callout
                    title={title}
                />
            </MapboxGL.PointAnnotation>
        );
    }

    const RenderAnnotations = () => {
        const items = [];
        for (let i = 0; i < cord.length; i++) {
            items.push(renderAnnotation(i));
        }
        return items;
    }

    return(
        <View style={styles.container}>
            <MapboxGL.MapView
            style={{flex : 1}}
            >
                <MapboxGL.UserLocation
                    visible={true}
                />
                <MapboxGL.Camera
                    followUserLocation={true}
                />
                <RenderAnnotations/>
            </MapboxGL.MapView>
        </View>
    );
}


const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },
});

export default Maps;