import React  from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Feathericon from 'react-native-vector-icons/Feather';
import Mci from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Homepage from './Home';
import Aboutpage from '../Tugas2/Biodata';
import Mapspage from './Maps';
import Chatpage from './Chat';

const Tab = createBottomTabNavigator();

const MainNavigation = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
                let iconName;
                if (route.name === 'Home') {
                    iconName = 'home-variant-outline';
                    return <Mci name={iconName} size={25} color={color} />;
                } else if (route.name === 'Maps') {
                    iconName = 'map-marker-radius';
                    return <Mci name={iconName} size={25} color={color} />;
                } else if (route.name === 'Chat') {
                    iconName = 'chatbubbles-outline';
                    return <Ionicons name={iconName} size={25} color={color} />;
                } else if (route.name === 'Profil') {
                    iconName = 'user';
                    return <Feathericon name={iconName} size={25} color={color} />;
                }
            }
        })}
        tabBarOptions={{
            activeTintColor: '#3EC6FF',
            inactiveTintColor: '#A4A4A4',
        }}
    >
                
    <Tab.Screen name="Home" component={Homepage} />
    <Tab.Screen name="Maps" component={Mapspage} />
    <Tab.Screen name="Chat" component={Chatpage} />
    <Tab.Screen name="Profil" component={Aboutpage} />
                                 
    </Tab.Navigator>
)

function AppTabNavigation({ navigation }) {
    return (
        <MainNavigation />
    )
}

export default AppTabNavigation;