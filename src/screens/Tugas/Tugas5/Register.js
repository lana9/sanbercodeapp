import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Modal,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import firebase from '@react-native-firebase/auth';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import Materialcommunity from 'react-native-vector-icons/MaterialCommunityIcons';


import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';

function Register({ navigation }) {

    const [isVisible, setIsvisible] = useState(false);
    const [type, setType] = useState('back');
    const [photo, setPhoto] = useState(null);

    const toggleCamera = () => {
        setType(type === 'back' ? 'front' : 'back');
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true}
        if(camera){
            const data = await camera.takePictureAsync(options);
            setPhoto(data);
            setIsvisible(false);
            console.log("=> "+data.uri);
        }
    }

    const uploadImage = (uri) => {
        const sessionId = new Date().getTime();
        return storage()
        .ref('images/'+sessionId)
        .putFile(uri)
        .then((response) => {
            alert("uppload success => "+response);
        })
        .catch((error) => {
            alert("error => " + error);
            console.log("error ===> "+error)
        })
    }

    const RenderCamera = () => {
        return(
            <Modal visible={isVisible} onRequestClose={() => setIsvisible(false)}>
                <View style={{flex : 1}}>
                    <RNCamera
                        style={{flex : 1}}
                        ref={ ref => {
                            camera = ref;
                        }}
                        type={type}
                    >
                        <View style={styles2.btnFlipContainer}>
                            <TouchableOpacity style={styles2.btnFlip} onPress={() => toggleCamera()}>
                                <Materialcommunity name="rotate-3d-variant" size={20} />
                            </TouchableOpacity>
                        </View>
                        <View  style={styles2.round}/>
                        <View  style={styles2.rectangle}/>
                        <View style={styles2.btnTakeContainer}>
                            <TouchableOpacity style={styles2.btnTake} onPress={() => takePicture()}>
                                <Icon name="camera" size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return(
        <View style={styles2.container}>
            <View style={styles2.header} >
                <Image
                    style={styles2.tinyLogo}
                    source={photo === null ? require('../../../assets/images/logo.jpg') : {uri : photo.uri}}
                />
                <TouchableOpacity onPress={() => setIsvisible(true)}>
                    <Text style={styles2.tittle} >Change Picture</Text>
                </TouchableOpacity>
                
            </View>
            <View style={styles2.footer} />
            <View style={styles2.text}>
                <View style={styles2.isi}>
                    <Text style={styles2.tbold}>Name</Text>
                    <TextInput style={styles2.tp}
                        underlineColorAndroid="#c6c6c6"
                        placeholder="Name"
                    />
                </View>
                <View style={styles2.isi}>
                    <Text style={styles2.tbold}>Email</Text>
                    <TextInput style={styles2.tp}
                        underlineColorAndroid="#c6c6c6"
                        placeholder="Email"
                    />
                </View>
                <View style={styles2.isi}>
                    <Text style={styles2.tbold}>Password</Text>
                    <TextInput style={styles2.tp}
                        underlineColorAndroid="#c6c6c6"
                        placeholder="Password"
                    />
                </View>
                
                <TouchableOpacity style={styles2.butlogout} onPress={() => uploadImage(photo.uri)}>
                    <Text style={{color: '#fff'}}>Register</Text>
                </TouchableOpacity>
            </View>
            <RenderCamera/>
        </View>
    )
}

const styles2 = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        flex: 1,
        backgroundColor:'#3EC6FF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flex: 2.5,
        backgroundColor: '#fff',
    },
    text: {
        height:hp('40%'),
        width:wp('85%'),
        backgroundColor: '#fff',
        position : "absolute",
        top: 190,
        left: 30,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        elevation: 5,
    },
    isi: {
        justifyContent: 'space-between',
        margin: 10
    },
    tinyLogo: {
        height:hp('12%'),
        width:wp('20%'),
        borderRadius: 100,
        margin:10
    },
    tittle: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },
    vbutlogout: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    butlogout: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3EC6FF',
        height:hp('5%'),
        width:wp('70%'),
        position : "absolute",
        top: hp('33%'),
        left: wp('8%'),
    },
    tp : {
        width: wp('80%')
    },
    tbold : {
        fontWeight: 'bold'
    },
    btnFlipContainer : {
        backgroundColor: '#fff',
        height:hp('5%'),
        width:wp('10%'),
        borderRadius: 100,
        top: hp('2%'),
        left: wp('5%'),
    },
    btnFlip : {
        justifyContent: 'center',
        alignItems: 'center',
        top: hp('1%'),
    },
    btnTakeContainer : {
        backgroundColor: '#fff',
        height:hp('10%'),
        width:wp('20%'),
        borderRadius: 100,
        top: hp('50%'),
        left: wp('40%'),
    },
    btnTake : {
        justifyContent: 'center',
        alignItems: 'center',
        top: hp('3%'),
    },
    round : {
        backgroundColor: '#fff',
        borderColor: '#fff',
        borderRadius: 1
    },
    rectangle : {
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: wp('1%'),
        width: wp('10%'),
        height: hp('20%'),
        margin: hp('1%')
    }
});

export default Register;