import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './Splashscreen';
import Intro from './Intro';
import Login from './Login';
import Register from '../Tugas5/Register';
import Tabnavigation from './tabnavigation';
import Chartgrafik from './Chartgrafik';

const Stack = createStackNavigator();

const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Tabnavigation" component={Tabnavigation} options={{ headerShown: false }} />
        <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
        <Stack.Screen name="Chartgrafik" component={Chartgrafik} options={{ headerShown: false }} />
    </Stack.Navigator>
);

function AppNavigation() {
    const [isLoading, setIsLoading] = React.useState(true);

    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000);
    }, []);

    if (isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )

}

export default AppNavigation;