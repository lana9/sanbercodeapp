import React from 'react';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

import Icon from 'react-native-vector-icons/Ionicons';

const slides = [
    {
        key: 1,
        title: 'Belajar Intensif',
        text: '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
        image: require('../../../assets/images/working-time.png'),
    },
    {
        key: 2,
        title: 'Teknologi Populer',
        text: 'Menggunakan bahasa pemrograman populer',
        image: require('../../../assets/images/research.png'),
    },
    {
        key: 3,
        title: 'From Zero to Hero',
        text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
        image: require('../../../assets/images/venture.png'),
    },
    {
        key: 4,
        title: 'Training Gratis',
        text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
        image: require('../../../assets/images/money-bag.png'),
    }
];

const Intro = ({navigation}) => {
    const renderItem = ({ item }) => {
        return (
            <View style={styles.slide}>
                <Text style={styles.title}>{item.title}</Text>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.text}>{item.text}</Text>
            </View>
        );
    }

    const onDone = () => {
        navigation.navigate('Login');
    }

    const renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    const renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Icon
                    name="arrow-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                />
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
            <View style={{ flex: 1 }}>
                {/* merender atau menjalankan library react-native-app-intro-slider */}
                <AppIntroSlider
                    data={slides}
                    onDone={onDone}
                    renderItem={renderItem}
                    renderDoneButton={renderDoneButton}
                    renderNextButton={renderNextButton}
                    keyExtractor={(item, index) => index.toString()}
                    activeDotStyle={{ backgroundColor: '#191970' }}
                />
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    slide: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    image: {
        width: 320,
        height: 320,
        marginVertical: 32,
    },
    text: {
        color: '#170e66',
        textAlign: 'center',
    },
    title: {
        fontSize: 22,
        color: '#170e66',
        textAlign: 'center',
        fontWeight: 'bold'
    },
    buttonCircle: {
        backgroundColor: '#170e66',
        width: 40,
        height: 40,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default Intro;