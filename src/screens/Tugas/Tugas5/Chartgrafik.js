import React, { useState } from 'react';
import {
    View,
    Text,
    processColor,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import {BarChart} from 'react-native-charts-wrapper';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';

const Chartgrafik = ({navigation}) => {

    const goBack = () => {
        navigation.navigate('Home');
    }

    const data = [
        {y:[100, 40], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[80, 60], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[40, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[78, 45], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[67, 87], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[98, 32], marker: ["React Native Dasar", "React Native Lanjutan"]},
        {y:[150, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
    ]

    const [legend, setLegend] = useState({
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: data,
                label: '',
                config: {
                    colors: [processColor('#2BD1F6'), processColor('#5880B6')],
                    stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu', 'Sep', 'Oct', 'Nov', 'Des'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        axisMinimum: -0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximum: new Date().getMonth() + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
    })

    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            enabled: false
        }
    })

    return (
        <View style={styles.container}>
            <View style={styles.navBar}>
                <TouchableOpacity onPress={() => goBack()} style={styles.iconstyle}>
                    <Icon name="arrow-left" size={25}/>
                    <Text style={styles.txstyle}>React Native</Text>
                </TouchableOpacity>
            </View>
            <BarChart
                style={{flex : 1}}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                chartDescription={{text : ''}}
                legend={legend}
                marker={{
                    enabled: true,
                    markerColor: 'red',
                    textColor: 'white',
                    textSize: 14
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        justifyContent: 'center',
    },
    iconstyle: {
        left : wp('5%'),
        flexDirection: 'row',
    },
    txstyle: {
        marginStart: wp('5%'),
        fontWeight : 'bold',
        fontSize: 20
     }
})

export default Chartgrafik;