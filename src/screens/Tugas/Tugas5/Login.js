import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import auth from '@react-native-firebase/auth';
import {CommonActions} from '@react-navigation/native';
import {GoogleSignin, statusCodes, GoogleSigninButton} from '@react-native-community/google-signin'

import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import TouchID from 'react-native-touch-id';
import api from '../../../api';


const config = {
    title : 'Authentication Required',
    imageColor : '#191970',
    imageErrorColor : 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription : 'Failed',
    cancelText : 'Cancel'
}

function Login({ navigation }) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem("token", token);
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignIn();
    }, []);

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId : '977854120559-3dao8i0k2e0h32orjdgvmfq06ounka7c.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const {idToken} = await GoogleSignin.signIn();
            console.log("idToken <===> "+idToken);

            const credential = auth.GoogleAuthProvider.credential(idToken);

            auth().signInWithCredential(credential);

            navigation.navigate('Tabnavigation');
        } catch (error) {
            console.log("error => "+error);
        }
    }

    const onLoginPress = () => {
        return auth().signInWithEmailAndPassword(email, password)
        .then((res) => {
            navigation.navigate('Tabnavigation');
        })
        .catch((err) => {
            console.log("==> "+err);
        })
    }

    // const onLoginPress = () => {
    //     let url = api+'/login';
    //     let data = {
    //         email: email,
    //         password: password
    //     }
    //     Axios.post(url, data, {
    //         timeout: 20000
    //     })
    //     .then((res) => {
    //         saveToken(res.data.token);
    //         navigation.navigate('Tabnavigation');
    //     })
    //     .catch((err) => {
    //         console.log("then ===> ", err)
    //     })
    // }

    const signWithFingerprint = () => {
        TouchID.authenticate('', config)
        .then(success => {
            alert("Authentication Success");
        })
        .catch(error => {
            alert("Authentication Failed" + error);
        })
    }

    const gotoRegister = () => {
        navigation.navigate('Register');
    }


    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#fff" barStyle="dark-content" />
            <Image source={require('../../../assets/images/logo.jpg')} style={styles.image} />
            
            <View style={styles.tt}>
                <Text style={styles.tisi}>Username</Text>
            </View>
            <View style={styles.up}>
                <TextInput style={styles.tp}
                    value = {email}
                    underlineColorAndroid="#c6c6c6"
                    placeholder="Username or Email"
                    onChangeText={(email) => setEmail(email)}
                />                
            </View>

            <View style={styles.tt}>
                <Text style={styles.tisi}>Password</Text>
            </View>
            <View style={styles.up}>
                <TextInput style={styles.tp}
                        secureTextEntry
                        value = {password}
                        underlineColorAndroid="#c6c6c6"
                        placeholder="Password"
                        onChangeText={(password) => setPassword(password)}
                    />
            </View>

            <View style={styles.btl}>
                <Button 
                color="#3EC6FF"
                title="LOGIN"
                onPress={() => onLoginPress()}
                />
            </View>

            <View style={{marginTop: 10}}>
                <GoogleSigninButton
                    onPress={() => signInWithGoogle()}
                    style={{width:'100%', height:40}}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}

                />
            </View>

            <View style={{marginTop: 10}}>
                <Button
                    color = "#191970"
                    title = "Sign in with Fingerprint"
                    onPress = {() => signWithFingerprint()}
                />
            </View>


            <View style={styles.tfooter}>
                <Text>Belum mempunyai akun ? </Text>
                <TouchableOpacity onPress={() => gotoRegister()}>
                    <Text style={{ color: "blue" }}
                    >Buat Akun</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },
    image: {
        width: wp('100%'),
        height: hp('30'),
        marginVertical: hp('5'),
    },
    up : {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: hp('1%'),
        marginBottom: hp('1%')
    },
    tp : {
        width: wp('90%')
    },
    tisi : {
        justifyContent: 'flex-start',
        alignContent: 'flex-start'
    },
    tt : {
        left: wp('6%')
    },
    btl : {
        width: wp('80%'),
        justifyContent: 'center',
        alignContent: 'center',
        left: wp('10%')
    },
    tfooter : {
        justifyContent: 'center',
        flexDirection: 'row',
        bottom: hp('-10%')
    }
});

export default Login;