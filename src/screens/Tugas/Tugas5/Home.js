import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
    StyleSheet,
    Dimensions,
    StatusBar,
    Image
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';


import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';


import FontAwesome from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { HeaderHeightContext } from '@react-navigation/stack';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

function Home({ navigation }) {

    const gotoChartgrafik = () => {
        navigation.navigate('Chartgrafik');
    }

    return(
        <ScrollView style={styles2.container}>
            <View style={styles2.container}>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={{padding: 10}}>
                    <View style={styles2.cardC}>
                        <View style={styles2.cardH}>
                            <Text style={styles2.cardTitle}>Kelas</Text>
                        </View>
                        <View style={styles2.cardCT}>
                            <View style={styles2.cardCTisi}>
                                <TouchableOpacity onPress={() => gotoChartgrafik()}>
                                    <Icon name="logo-react" size={55} color={Colors.white}/>
                                    <Text style={styles2.cardTitle}>React Native</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <Icon name="logo-python" size={55} color={Colors.white}/>
                                <Text style={styles2.cardTitle}>Detail Science</Text>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <Icon name="logo-react" size={55} color={Colors.white}/>
                                <Text style={styles2.cardTitle}>React JS</Text>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <Icon name="logo-laravel" size={55} color={Colors.white}/>
                                <Text style={styles2.cardTitle}>Laravel</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles2.cardC}>
                        <View style={styles2.cardH}>
                            <Text style={styles2.cardTitle}>Kelas</Text>
                        </View>
                        <View style={styles2.cardCT}>
                            <View style={styles2.cardCTisi}>
                                <Icon name="logo-wordpress" size={55} color={Colors.white}/>
                                <Text style={styles2.cardTitle}>Wordpress</Text>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <Image
                                    style={styles2.tinyLogo}
                                    source={require('../../../assets/images/website-design.png')}
                                />
                                <Text style={styles2.cardTitle}>Design Grafis</Text>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <FontAwesome name="server" size={55} color={Colors.white}/>
                                <Text style={styles2.cardTitle}>Web Server</Text>
                            </View>
                            <View style={styles2.cardCTisi}>
                                <Image
                                    style={styles2.tinyLogo2}
                                    source={require('../../../assets/images/ux.png')}
                                />
                                <Text style={styles2.cardTitle}>UI/UX Design</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles2.cardC}>
                        <View style={styles2.cardH}>
                            <Text style={styles2.cardTitle}>Summary</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>React Native</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>20 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>Data Science</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>30 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>ReactJS</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>66 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>Laravel</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>60 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>Wordpress</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>40 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>Design Grafis</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>50 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>Web Server</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>55 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>

                    <View style={styles2.cardC1}>
                        <View style={styles2.cardH1}>
                            <Text style={styles2.cardTitle}>UI/UX Design</Text>
                        </View>
                    </View>
                    <View style={styles2.cardC3}>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Today</Text>
                            <Text style={styles2.cardTitle}>45 Orang</Text>
                        </View>
                        <View style={styles2.cardC2}>
                            <Text style={styles2.cardTitle}>Total</Text>
                            <Text style={styles2.cardTitle}>100 Orang</Text>
                        </View>
                    </View>
                    
                </View>
            </View>
        </ScrollView>
    )
}

const styles2 = StyleSheet.create({
    container: {
        flex: 1,
    },
    cardC1: {
        flexDirection: 'column',
    },
    cardC2: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    cardC3: {
        backgroundColor: '#088dc4',
    },
    cardH1: {
        flex: 1,
        backgroundColor: '#3EC6FF',
    },
    cardC: {
        flexDirection: 'column',
        marginTop: 5
    },
    cardH: {
        flex: 1,
        backgroundColor: '#088dc4',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
    },
    cardTitle: {
        margin: 2,
        color: '#fff'
    },
    cardCT: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#3EC6FF'
    },
    cardCTisi: {
        flexDirection: 'column',
        alignItems: 'center',
        margin:10
    },
    tinyLogo: {
        height:hp('7.5%'),
        width:wp('18%'),
    },
    tinyLogo2: {
        height:hp('7%'),
        width:wp('14%'),
    },
});


export default Home;