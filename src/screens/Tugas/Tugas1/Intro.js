import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import style1 from '../../../style/style1';


const Intro = () => {
    return (
        <View style={style1.container}>
            <Text>Hallo Kelas React Lanjutan Sanbercode!</Text>
        </View>
    )
}

export default Intro;