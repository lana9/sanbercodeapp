import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

import style2 from '../../../style/style2';

import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin'
import api from '../../../api';


const Biodata = ({ navigation }) => {

    const [userInfo, setUserInfo] = useState(null); 

    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem("token");
            } catch (err) {
                console.log("err => "+err)
            }
        }
        getToken();
        getCurrentUser();
    }, []);

    const getCurrentUser = async () => {
        const userInfo = await GoogleSignin.signInSilently();
        console.log("ui <==> "+userInfo.user.photo);
        setUserInfo(userInfo);
    }

    const getVenue = (token) => {
        let url = api+'/venues';
        Axios.get(url, {
            timeout: 20000,
            headers: {
                'Authorization' : 'Bearer' + token
            }
        })
        .then((res) => {
            console.log("res => "+res);
        })
        .catch((err) => {
            console.log("err => "+err);
        })
    }

    const onLogoutPress = async () => {
        try{
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            await Asyncstorage.removeItem("token");
            navigation.navigate('Login');
        } catch (err) {
            console.log(err)
        }
    }

    return(
        <View style={style2.container}>
            <View style={style2.header} >
                <Image
                    style={style2.tinyLogo}
                    source={{
                    uri: userInfo && userInfo.user && userInfo.user.photo
                    }}
                />
                <Text style={style2.tittle} >{userInfo && userInfo.user && userInfo.user.name}</Text>
            </View>
            <View style={style2.footer} />
            <View style={style2.text}>
                <View style={style2.isi}>
                    <Text>Tanggal Lahir</Text>
                    <Text>9 Agustus 1994</Text>
                </View>
                <View style={style2.isi}>
                    <Text>Jenis Kelamin</Text>
                    <Text>Laki-laki</Text>
                </View>
                <View style={style2.isi}>
                    <Text>Hobi</Text>
                    <Text>Ngulik</Text>
                </View>
                <View style={style2.isi}>
                    <Text>No. Telp</Text>
                    <Text>087871370599</Text>
                </View>
                <View style={style2.isi}>
                    <Text>Email</Text>
                    <Text>{userInfo && userInfo.user && userInfo.user.email}</Text>
                </View>
                <TouchableOpacity style={style2.butlogout} onPress={() => onLogoutPress()}>
                    <Text style={{color: '#fff'}}>LOGOUT</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

export default Biodata;