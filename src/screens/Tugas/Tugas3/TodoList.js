import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native';

import style3 from '../../../style/style3';

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const TodoList = () => {

    const [todos, setTodos] = useState([]);
    const [ind, setInd] = useState(0);
    const [tgl, setTgl] = useState();
    const [isian, setIsian] = useState('');

    const changeIsian = (val) => {
        setIsian(val);
    }

    const tambah = () => {
        setInd(ind+1);

        setTodos([...todos, {text:isian, key:ind+''}]);

        let tngl = new Date().getFullYear()+"/"+(new Date().getMonth()+1)+"/"+new Date().getDay();
        setTgl(tngl);
    }

    const hapus = (key) => {
        setTodos((prevTodos) => {
            return prevTodos.filter(todo => todo.key != key)
        });
    }

    return(
        <View style={style3.container}>
            <View style={style3.sduhurs}>
                <Text style={style3.sduhurs}>Masukan Todolist</Text>
                <View style={style3.duhurs}>
                    <TextInput
                        style={style3.stext}
                        placeholder='Input here'
                        onChangeText={changeIsian}
                    />
                    <TouchableOpacity style={style3.appButtonContainer2} onPress={() => tambah()}>
                        <Ionicons name={'add'} size={20} />
                    </TouchableOpacity>
                    
                </View>
            </View>
            <View>
                <FlatList
                    data={todos}
                    renderItem={({item}) => (
                        <View style={style3.vheader} key={item.key}>
                            <TouchableOpacity
                                    onPress={() => hapus(item.key)}>
                            <View style={style3.vns}>
                                <View style={style3.vn}>
                                    <Text>{tgl}</Text>
                                    <Text>{item.text}</Text>
                                </View>
                                <FontAwesome name={'trash-o'} size={20} />
                            </View>
                            
                            </TouchableOpacity>
                        </View>
                    )}
                />
            </View>
            
        </View>
    )
}

export default TodoList;