import React, {useState, createContext} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {

    const [input, setInput] = useState('');
    const [todos, setTodos] = useState([]);
    const [ind, setInd] = useState(0);
    const [tgl, setTgl] = useState();

    handleChangeInput = (value) => {
        setInput(value);
    }

    addTodo = () => {
        if(input != ''){
            setInd(ind+1);

            let d = new Date();
            const saiki = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
            setTgl(saiki);

            setTodos([...todos, {title: input, key: ind+''}]);
            setInput('');
        }else{
            alert('Input data dulu');
        }
        
    }

    removeTodo = (key) => {
        setTodos((prevTodos) => {
            return prevTodos.filter(todo => todo.key != key)
        });
    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            tgl,
            handleChangeInput,
            addTodo,
            removeTodo
        }}>
            <TodoList/>
        </RootContext.Provider>
    )

}

export default Context;