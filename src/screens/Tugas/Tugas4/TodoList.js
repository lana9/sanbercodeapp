import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import style4 from '../../../style/style4';

import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {RootContext} from '../Tugas4';


const TodoList = () => {

    const state = useContext(RootContext);

    const renderItem = ({item}) => {
        return(
            <View style={style4.vheader} key={item.key}> 
                <View style={style4.vns}>
                    <View style={style4.vn}>
                        <Text>{state.tgl}</Text>
                        <Text>{item.title}</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() => state.removeTodo(item.key)}>
                        <FontAwesome name={'trash-o'} size={20} />
                    </TouchableOpacity>
                </View>
                            
            </View>
        )
    }

    return(
        <View style={style4.container}>
            <View style={style4.sduhurs}>
                <Text style={style4.sduhurs}>Masukan Todolist</Text>
                <View style={style4.duhurs}>
                    <TextInput
                        style={style4.stext}
                        value={state.input}
                        placeholder='Input here'
                        onChangeText={(value) => state.handleChangeInput(value)}
                    />
                    <TouchableOpacity style={style4.appButtonContainer2} onPress={() => state.addTodo()}>
                        <Ionicons name={'add'} size={20} />
                    </TouchableOpacity>
                    
                </View>
            </View>
            <View>
                <FlatList
                    data={state.todos}
                    renderItem={renderItem}
                />
            </View>
            
        </View>
    )
}

export default TodoList;