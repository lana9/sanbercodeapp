import {
    StyleSheet
} from 'react-native';

const styles3 = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    stext : {
        height: 40,
        width: 350,
        borderColor: 'black',
        borderWidth: 1,
    },
    appButtonContainer2: {
        backgroundColor: "powderblue",
        paddingVertical: 10,
        paddingHorizontal: 12,
        width: 50,
    },
    appButtonContainer3: {
        backgroundColor: "#003366",
        paddingVertical: 10,
        paddingHorizontal: 12,
        position: "absolute",
        width: 50,
        left: 135,
        top: 500
    },
    appButtonText2: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center"
    },
    vheader: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    vn: {
        flexDirection:'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    duhurs: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
    },
    sduhurs: {
        margin:3,
        justifyContent: 'flex-end',
        flexDirection: 'column',
    },
    vns: {
        backgroundColor: "#fff",
        paddingVertical: 10,
        paddingHorizontal: 12,
        width: 400,
        borderWidth: 3,
        borderColor: '#D0D1D2',
        margin: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
});

export default styles3;