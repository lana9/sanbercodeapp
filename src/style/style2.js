import {
    StyleSheet
} from 'react-native';

import {
    widthPercentageToDP as wp, heightPercentageToDP as hp
} from 'react-native-responsive-screen';


const styles2 = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        flex: 1,
        backgroundColor:'#3EC6FF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        flex: 2.5,
        backgroundColor: '#fff',
    },
    text: {
        height:hp('30%'),
        width:wp('85%'),
        backgroundColor: '#fff',
        position : "absolute",
        top: 190,
        left: 30,
        borderTopStartRadius: 10,
        borderTopEndRadius: 10,
        borderBottomStartRadius: 10,
        borderBottomEndRadius: 10,
        elevation: 5,
    },
    isi: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 10
    },
    tinyLogo: {
        height:hp('12%'),
        width:wp('20%'),
        borderRadius: 100,
        margin:10
    },
    tittle: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold'
    },
    vbutlogout: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    butlogout: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3EC6FF',
        height:hp('5%'),
        width:wp('70%'),
        position : "absolute",
        top: hp('24%'),
        left: wp('8%'),
    }
});

export default styles2;