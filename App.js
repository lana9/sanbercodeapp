/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import firebase from '@react-native-firebase/app';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

FontAwesome.loadFont();
FontAwesome5.loadFont;

// import Intro from './src/screens/Tugas/Tugas1/Intro';
// import Biodata from './src/screens/Tugas/Tugas2/Biodata';
// import Todolist from './src/screens/Tugas/Tugas3/TodoList';
// import Index from './src/screens/Tugas/Tugas4';
import Navigation from './src/screens/Tugas/Tugas5/navigation';


var firebaseConfig = {
  apiKey: "AIzaSyDZkzAXQB4iFX396T4JMz0STFMmoUTfoFc",
  authDomain: "sanbercodelana.firebaseapp.com",
  databaseURL: "https://sanbercodelana.firebaseio.com",
  projectId: "sanbercodelana",
  storageBucket: "sanbercodelana.appspot.com",
  messagingSenderId: "977854120559",
  appId: "1:977854120559:web:cca0b2f9b933cd9059439a",
  measurementId: "G-D68N95359Q"
};
// Inisialisasi firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}


const App: () => React$Node = () => {
  return (
    // <Intro/>
    // <Biodata/>
    // <Todolist/>
    // <Index/>
    <Navigation/>
  );
};

export default App;
